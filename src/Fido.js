import React, { Component } from 'react'
import {
    View,
    StyleSheet
} from 'react-native'
import { MainFeed, Login, Register, Profile, Camera } from './components/screens';
import { SwitchNavigator, TabNavigator, StackNavigator} from 'react-navigation'

const TabsStack = TabNavigator({
    feed: MainFeed,
    camera: Camera,
    profile: Profile
})

const IntroStack = StackNavigator({
    register: Register,
})

const MainStack = SwitchNavigator({
    login: Login,
    register: Register,
    main: TabsStack
})

class Fido extends Component {
    render() {
        return <MainStack />;
    }
}

export default Fido;