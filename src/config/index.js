export default {
    images: {
        heartIcon: require('../../assets/heart.png'),
        chatIcon: require('../../assets/chat.png'),
        arrowIcon: require('../../assets/arrow.png')
    },

    styleConstant: {
        rowHeight: 50
    },

    baseUrl: "http://fidoapi-1u5hhv.turbo360-vertex.com/api/"
};