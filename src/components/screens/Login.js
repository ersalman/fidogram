import React, { Component } from 'react'
import {
View,
Text,
TextInput,
Button,
StyleSheet,
TouchableOpacity,
StatusBar
} from 'react-native'

class Login extends Component {
constructor() {
    super();
    this.state = {
        credentials: {
            email: "",
            password: ""
        }
    }
}

login() {
    fetch(config.baseUrl + 'login', {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
        body: JSON.stringify(this.state.credentials),
})
.then(response => response.json())
.then(jsonResponse => {
    //alert(JSON.stringify(jsonResponse));
    console.log(JSON.stringify(jsonResponse));

    if (jsonResponse.confirmation === 'success') {
        this.props.navigation.navigate('main');
    } else {
        throw new Error({
            message: "Sorry, something went wrong, Please try again."
        });
    }
    //
}).catch(err => {
    alert(err)
});
}

updateText(text, field) {
    let newCredentials = Object.assign(this.state.credentials);
    newCredentials[field] = text
    this.setState({
        credentials: newCredentials
    });
}

render() {
    return (
        <View style = {styles.root} >
            <StatusBar barStyle = 'light-content'/>
                <View style = {styles.header}>
                    <Text style = {styles.appName}>FiDOGRAM</Text>
                </View >
                <View style = {styles.content}>
                    <View style = {styles.section}>
                        <View style = {styles.inputWrapper}>
                            <TextInput 
                            value = {this.state.email}
                            onChangeText = {text => this.updateText(text, 'email')}
                            autoCorrect = {false}
                            placeholder = 'Email'
                            style = {styles.input}/>
                        </View>
                        <View style = {styles.inputWrapper}>
                            <TextInput
                            value = {this.state.password}
                            secureTextEntry
                            autoCorrect = {false}
                            onChangeText = {text => this.updateText(text, 'password')}
                            placeholder = 'Password'
                            style = {styles.input}/>
                        </View>
                            <TouchableOpacity style = {styles.loginButton}
                            onPress = {() => {
                                this.login();
                            }}
                            >
                            <Text>Login</Text>
                            </TouchableOpacity>
                            <Button title = 'No account?, Sign up here!' onPress = {() => this.props.navigation.navigate('register')}/>
                    </View>

                    <View style = {styles.orWrapper}>
                        <View style = {styles.orDivider}/>
                            <View style = {styles.orTextWrapper}>
                            <Text style = {styles.orText}>OR</Text>
                            </View>
                        <View style = {styles.orDivider}/>
                    </View>
                    <View style = {styles.section}/>
              </View>
        </View>
    )
}
}

const styles = StyleSheet.create({
root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
},

appName: {
    fontSize: 40,
    color: 'white'
},

header: {
    flex: 0.3,
    backgroundColor: 'purple',
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center'
},

content: {
    height: 100 + "%",
    width: 100 + "%",
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
},

section: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
},

inputWrapper: {
    height: 45, 
    width: '90%',
    borderColor: '#E4E4E4',
    backgroundColor: '#F4F9F9',
    borderRadius: 5,
    borderWidth: 1,
    marginBottom: 10,
    padding: 10

},
loginButton:{
    height: 45, 
    width: '90%',
    borderColor: '#E4E4E4',
    backgroundColor: '#318DEE',
    borderRadius: 5,
    borderWidth: 1,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
},
input: {
    flex: 1,
},
orWrapper: {
    width: '90%',
    marginVertical: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center'
},
orDivider: {
    height: 1,
    flex: 1,
    width: '100%',
    backgroundColor: '#E4E4E4'
},

orTextWrapper: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
},
orText: {
    color: 'gray'
}
})

export default Login