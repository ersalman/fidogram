import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native'

class Camera extends Component {
    login() {
         this.props.navigation.navigate('main');
    }

    render() {
        return (
            <TouchableOpacity 
            style = {styles.loginmain}
            onPress = {() => {
                this.login();
            }} >
                <Text>Camera</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    loginmain: {
        height: 100 + "%",
        width: 100 + "%",
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default Camera