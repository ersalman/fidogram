import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet
} from 'react-native'

import config from '../../config';
import { PostFeed } from '../container';

class MainFeed extends Component {
    render() {
        return (
            <View style = {{ flex: 1, width: 100 + "%", height: 100 + "%"}}> 
                 <View style = {styles.topNavBar}>
                      <Text>FiDO</Text>
                 </View>
                 <PostFeed/>
            </View>
        );
    }
}

const styles = StyleSheet.create ({
     topNavBar: {
        width: 100 + "%",
        height: 56,
        backgroundColor: 'rgb(250,250,250)',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: 'rgb(233,233,233)',
        justifyContent: "center",
        alignItems: "center",
        marginTop: 20
       }  
})

export default MainFeed;