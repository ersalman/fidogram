import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Button,
    StatusBar
} from 'react-native'

import config from '../../config'

class Register extends Component {

    constructor() {
        super();
        this.state = {
            credentials: {
                email: "",
                password: ""
            }
        }
    }

register() {
     fetch(config.baseUrl + 'signup', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
       },
         body: JSON.stringify(this.state.credentials),
    })
    .then(response => response.json())
    .then(jsonResponse => {
        //alert(JSON.stringify(jsonResponse));
        console.log(JSON.stringify(jsonResponse));

        if (jsonResponse.confirmation === 'success') {
            this.props.navigation.navigate('main');
        } else {
            throw new Error({
                message: "Sorry, something went wrong, Please try again."
            });
        }
        //
    }).catch(err => {
        alert(err)
    });
 }

    updateText(text, field) {
        let newCredentials = Object.assign(this.state.credentials);
        newCredentials[field] = text
        this.setState({
            credentials: newCredentials
        });
    }

    render() {
        return (
            <View style = {styles.root} >
            <StatusBar barStyle = 'light-content'/>
                <View style = {styles.header}>
                    <Text style = {styles.appName}>FiDOGRAM</Text>
                </View >
                <View style = {styles.content}>
                <TextInput 
                value = {this.state.email}
                onChangeText = {text => this.updateText(text, 'email')}
                autoCorrect = {false}
                placeholder = 'Email'
                style = {styles.input}/>
                <TextInput
                value = {this.state.password}
                secureTextEntry
                autoCorrect = {false}
                onChangeText = {text => this.updateText(text, 'password')}
                placeholder = 'Password'
                style = {styles.input}/>
                <Button
                    onPress = {() => {
                        this.register();
                    }}
                    title = 'Signup'
                />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    appName: {
        fontSize: 40
    },

    header: {
        flex: 0.3,
        backgroundColor: 'purple',
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center'
    },

    content: {
        height: 100 + "%",
        width: 100 + "%",
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'red'
    },

    input: {
        height: 40, 
        width: 100 + '%', 
        paddingHorizontal: 50,
        backgroundColor: 'rgb(255,255,255)',
        marginBottom: 10
    }
})

export default Register