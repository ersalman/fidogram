import React, { Component } from 'react'
import { Feed } from '../presentation'
import { FlatList } from 'react-native'


class PostFeed extends Component {

    _renderPost({ item }) {
        return <Feed />
    }

    _returnKey(item) {
        return item.toString()
    }

    render() {
    return  <FlatList 
                data = {[1,2,3,4,5,6,7,8,9]}
                renderItem = { this._renderPost() }
                keyExtractor = { this._returnKey }
        /> 
    }
}

export default PostFeed