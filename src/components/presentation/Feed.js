import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    TouchableOpacity
} from 'react-native'

import config from "../../config"

class Feed extends Component {

    constructor() {
        super()
        this.state = {
            liked: false,
            screenWidth: Dimensions.get('window').width
        };
    }

    likedToggled() {
        this.setState({
            liked: !this.state.liked
        })
    }

    render() {

        const imageHeight = Math.floor(this.state.screenWidth * 1.1)
        const heartTintColor = this.state.liked ? 'rgb(262,61,57)' : null

        return (
            <View style = {{ flex: 1, width: 100 + '%' }}>
                <View style = {styles.userBar}>
                    <View style = {{flexDirection: 'row', alignItems: 'center'}}>
                         <Image style = {styles.userPicture}
                             source = {{ uri: 'https://lh3.googleusercontent.com/EUT1ioPdMH8S8KpAi6E3H0iewtg1n9LqN29L_xDZsRyRV505UX1VPhRi9jnCSQ1L-TsZYTJ_f2JSym2ZD4QnnMkZgHc'}}
                         />
                         <Text style = {{ marginLeft: 10 }}>Steve Jobs</Text>
                    </View>
                    <View style = {{ alignItems: 'center' }}>
                        <Text style = {{ fontSize: 30 }}>...</Text>
                    </View>
                 </View>

                <TouchableOpacity
                    activeOpacity = {0.9}
                    onPress = { () => {
                        this.likedToggled();
                    }}
                >
                 <Image
                    style = {{width: this.state.screenWidth, height: imageHeight}} 
                    source = {{uri: 'https://lh3.googleusercontent.com/F4a1kxS6dCO9z9JztnjPzoLHRTKMaEtmVgBNObbIg-FuxULDrz_Bx8fwJ79tZt2n0M5-YcEZ6zkUOveeZjWX3bcULw'}}/>

                </TouchableOpacity>

                <View style = {styles.iconBar}>
                    <Image style = {[styles.icon, { width: 40, height: 40, tintColor: heartTintColor }]} source = {config.images.heartIcon}/>
                    <Image style = {[styles.icon, { width: 34, height: 34 }]} source = {config.images.chatIcon}/>
                    <Image style = {[styles.icon, { width: 40, height: 30 }]} source = {config.images.arrowIcon}/>
                </View>

                <View style = { styles.iconBar }>
                    <Image style = {[styles.icon, { width: 30, height: 30 }]} source = {config.images.heartIcon}/>
                    <Text> 128 Likes</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create ({
     topNavBar: {
        width: 100 + "%",
        height: 56,
        backgroundColor: 'rgb(250,250,250)',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: 'rgb(233,233,233)',
        justifyContent: "center",
        alignItems: "center",
        marginTop: 20
       },
       userBar: {
        width: 100 + "%",
        height: config.styleConstant.rowHeight,
        backgroundColor: 'rgb(255,255,255)',
        flexDirection: 'row',
        paddingHorizontal: 10,
        justifyContent: "space-between"
       },
       userPicture: {
        width: 40,
        height: 40,
        borderRadius: 20
       },
       iconBar: {
        width: 100 + "%",
        height: config.styleConstant.rowHeight,
        borderTopWidth: StyleSheet.hairlineWidth,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: 'rgb(233,233,233)',
        flexDirection: 'row',
        alignItems: 'center',
       },
       icon: {
           marginLeft: 10
       }
})

export default Feed;